import logging
import sys
from card import Card

# TODO: Add exception handling

# split on spaces
# a [m, d] [r, g, b, y, w] [0-9]        add to stack
# r [m, d] [r, g, b, y, w] [0-9]        remove from stack
# c [m, d]                              clear stack
# p [m, d]                              print stack


def parse(cmd, myStack, discardStack):
    while (len(cmd) != 4):
        cmd.append(" ")

    d = {"instruction": cmd[0][0], "stack": cmd[1]
         [0], "color": cmd[2][0], "value": cmd[3][0]}
    if (cmd[0][0] == "a" or cmd[0][0] == "+"):
        return addCard(d, myStack, discardStack)
    if (cmd[0][0] == "r" or cmd[0][0] == "-"):
        return removeCard(d, myStack, discardStack)
    if (cmd[0][0] == "c"):
        return clear(d, myStack, discardStack)

    if (d["instruction"] == "p"):
        return printStack(d, myStack, discardStack)
    logging.error("Unknown instruction %e", d["instruction"])
    return 1


def addCard(d, myStack, discardStack):
    if (d["stack"] == "m"):
        myStack.append(Card(d["color"], d["value"]))
        return 0
    if (d["stack"] == "d"):
        discardStack.append(Card(d["color"], d["value"]))
        return 0

    logging.error("Unknown stack %s", d["stack"])
    return 1


def removeCard(d, myStack, discardStack):
    if (d["stack"] == "m"):
        try:
            myStack.remove(Card(d["color"], d["value"]))
            return 0
        except ValueError:
            logging.warning("%s %s is not in my stack", d["color"], d["value"])
            return 1

    for i, e in reversed(list(enumerate(discardStack))):
        if(e.color == d["color"] and e.value == d["value"]):
            discardStack.pop(i)
            return 0
    if (d["stack"] == "d"):
        logging.warning("%s %s is not in the discard stack",
                        d["color"], d["value"])

    logging.error("Unknown stack %s", d["stack"])
    return 1


def clear(d, myStack, discardStack):
    if (d["stack"] == "m"):
        myStack.clear()
        return 0
    if (d["stack"] == "d"):
        discardStack.clear()
        return 0

    return 1


def printStack(d, myStack, discardStack):
    if (d["stack"] == "m"):
        for i in myStack:
            print(i)
        return 0
    if (d["stack"] == "d"):
        for i in reversed(discardStack):
            print(i)
        return 0

    return 1
