class Card:
    # color: green, red, yellow, blue, wild
    # value: 0-9, skip, reverse, +2, +4, multicolor, shuffle

    # TODO: Add exception handling
    def __init__(self, color, value):
        self.color = color.lower()
        self.value = value.lower()
        if self.color == "w":
            self.isWildCard = True
        else:
            self.isWildCard = False

    def __str__(self):
        return "" + self.color + " " + self.value

    def __eq__(self, other):
        return (self.color, self.value) == (other.color, other.value)
